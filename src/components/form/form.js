import "./form.css"
import Input from "../input/intput";
import OptionList from "../optionList/optionList.js";
import Button from "../button/button";
import { useState } from "react";

const Form = (props) => {

    let {teams,setWorker,addTeam} = props;

    const handleSubmit = (event)=>{
        event.preventDefault();
        let data = {
            name:name,
            position:position,
            photo:photo,
            team:team,
        }
        setWorker(data)
    }


    const handleAdd = (event) =>{
        event.preventDefault();
        let data ={
            title,
            basicColor:color,
        }
        addTeam(data);
    }

    const [name,setName] = useState("");
    const [position,setPosition] = useState("");
    const [photo,setPhoto] = useState("");
    const [team,setTeam] = useState("");
    const [title,setTitle] = useState("");
    const [color,setcolor] = useState("");

    return <section className="form">
        <form onSubmit={handleSubmit}>
            <h2>Rellena el formulario para crear el colaborador.</h2>
            <Input 
                title="Nombre" 
                placeholder="ingresar nombre" 
                required
                inputValue={name}
                setValue={setName}
            />
            <Input 
                title="Puesto" 
                placeholder="ingresar puesto" 
                required
                inputValue={position}
                setValue={setPosition}
            />
            <Input 
                title="Foto" 
                placeholder="ingresar enlace de la foto" 
                required
                inputValue={photo}
                setValue={setPhoto}
            />
            <OptionList
                groups= {teams}
                optionValue= {team}
                setValue= {setTeam}
            />
            <Button>Crear</Button>
        </form>
        <form onSubmit={handleAdd}>
            <h2>Rellena el formulario para crear el equipo.</h2>
            <Input 
                title="Titulo" 
                placeholder="ingresar nombre" 
                required
                inputValue={title}
                setValue={setTitle}
            />
            <Input 
                title="color" 
                placeholder="ingresar puesto" 
                required
                inputValue={color}
                setValue={setcolor}
                type = "color"
            />
            <Button>Crear</Button>
        </form>
    </section>
}
export default Form;