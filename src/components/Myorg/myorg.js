
import "./myorg.css"
import { useState } from "react";
const Myorg = (props) =>{
    // Estado - hooks
    // useState()
    // const [mostrar, actulizarMostrat] = useState(true);
    // const handleClick = ()=>{
    //     // actulizarMostrat(!mostrar)
    // }; 

    return <section className="org-section">
        <h3 className="org-section__title">Mi organización</h3>
        <img className="org-section__add" onClick={props.toggleShow} src="/image/add.png" alt="add"/>
    </section>
}

export default Myorg;