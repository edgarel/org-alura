import "./optionList.css"

const OptionList = (props) => {

    const handleChange = (event) =>{
        props.setValue(event.target.value)
    }

    return <div className="option-list">
        <label>Equipos</label>
        <select value={props.optionValue} onChange={handleChange}>
            <option value="" disabled defaultValue="" hidden>
                Seleccionar grupo
            </option>
            {
                props.groups.map((team,index) => <option key={index}>{team}</option>)
            }
        </select>
    </div>  
};

export default OptionList;