import "./coworker.css";
import {AiFillCloseCircle, AiFillHeart, AiOutlineHeart} from "react-icons/ai";

const CoWorker = (props) =>{

    let {id,photo,name,position,like} = props.dataCoworker;
    let {basicColor,deleteCoworker,giveLike} = props;
    
    const obj = {
        "--background-higligth": `${basicColor}`
    }

    return <section className="coworker" style={obj} >
        <AiFillCloseCircle className="coworker__delete" onClick={()=>deleteCoworker(id)}/>
        <img className="coworker__image" src={photo} alt=""/>
        <div className="coworker__info">
            <h3 className="coworker__name">{name}</h3>
            <p className="coworker__job">{position}</p>
            {
                like ? <AiFillHeart color="red" onClick={()=>{giveLike(id)}}/> : <AiOutlineHeart onClick={()=>{giveLike(id)}}/>
            }
        </div>
    </section>
}
export default CoWorker;

{/* <span className="coworker__close" onClick={deleteCoworker}>x</span> */}
