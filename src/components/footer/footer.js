import "./footer.css"

const Footer = ()=>{

    const backgroundImage = {
        "--image":"url('/image/Footer.png')"
    }

    return <footer className="endboard" style={backgroundImage}>
        <div className="endboard__social">
            <a href="#">
                <img src="/image/icon/facebook.png" alt=""/>
            </a>
            <a href="#">
                <img src="/image/icon/twitter.png" alt=""/>
            </a>
            <a href="#">
                <img src="/image/icon/instagram.png" alt=""/>
            </a>
        </div>
        <img className="enboard__logo" src="/image/Logo.png" alt=""/>
        <strong className="endborad__credits">Diseño Alura</strong>
    </footer>
}

export default Footer;