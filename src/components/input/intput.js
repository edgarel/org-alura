import "./input.css"
import { useState } from "react"
const Input = (props) =>{

    // const [valor,setValor] = useState("")
    // console.log("Datos: ", props)

    const {title, placeholder, required, inputValue , type = "text"} = props

    const handleChange = (event) =>{
        props.setValue(event.target.value)
    }

    return <div className={`input input--${type}`}>
        <label>{title}</label>
        <input 
            type={type} 
            name="" 
            id="" 
            placeholder={placeholder} 
            required={required}
            value={inputValue}
            onChange={handleChange}
        />
    </div>
}

export default Input;