import "./team.css"
import CoWorker from "../coworker/coworker.js";
const Team = (props) =>{

    let {id,title,basicColor} = props.dataTeam;
    let {coworkers,deleteCoworker,updateColor, giveLike} = props;

    let styleTeam = { backgroundColor: basicColor.concat("80")}
    let styleTitle = { borderColor: basicColor}

    return <>
        { 
            coworkers.length > 0 &&
                <section className="team" style={styleTeam}>
                    <input
                        type="color"
                        className="team__color"
                        value={basicColor}
                        onChange = { (event) => {
                                updateColor(event.target.value,id)
                            }
                        }
                    />
                    <h3 className="team__title" style={styleTitle}>{title}</h3>
                    <div className="team__list">
                    {
                        coworkers.map((coworker,index)=> <CoWorker key={index} 
                                dataCoworker   = {coworker} 
                                basicColor  = {basicColor}
                                deleteCoworker = {deleteCoworker}
                                giveLike = {giveLike}
                            /> 
                        )
                    }
                    </div>
                </section>
        }
    </>
}

export default Team;