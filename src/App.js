import './App.css';
import Header from './components/header/header.js';
import Form from './components/form/form.js';
import Myorg from './components/Myorg/myorg.js';
import Team from './components/team/team.js';
import Footer from './components/footer/footer.js';

import {v4 as uuid} from 'uuid';

import { useState } from "react";

function App() {

  let person = [
    {
      id: uuid(),
      name: "Genesys Rondón",
      photo: "https://github.com/genesysaluralatam.png",
      position: "DesarrolladorDesarrolladora de software e instructora",
      team: "Programación",
      like: true,
    },
    {
      id: uuid(),
      name: "Jeanmarie Quijada",
      photo: "https://github.com/JeanmarieAluraLatam.png",
      position: "Instructora en Alura Latam",
      team: "Front End",
      like: false,
    },
    {
      id: uuid(),
      name: "Christian Velasco",
      photo: "https://github.com/christianpva.png",
      position: "Head de Alura e instructor",
      team: "Data Science",
      like: false,
    },
    {
      id: uuid(),
      name: "Jose Gonzalez",
      photo: "https://github.com/JoseDarioGonzalezCha.png",
      position: "Dev. FullStack",
      team: "Devops",
      like: true,
    },
    {
      id: uuid(),
      name: "Harland Lohora",
      photo: "https://github.com/harlandlohora.png",
      position: "Desarrollador",
      team: "UX y Diseño",
      like: false,
    },
    {
      id: uuid(),
      name: "Genesis",
      photo: "https://github.com/genesysaluralatam.png",
      position: "Desarrollador",
      team: "Móvil",
      like: false,
    },
    {
      id: uuid(),
      name: "Genesis",
      photo: "https://github.com/genesysaluralatam.png",
      position: "Desarrollador",
      team: "Innovación y Gestión",
      like: false,
    }
]

let teams = [
  {id: uuid(), title:"Programación", basicColor:"#57C278"},
  {id: uuid(), title:"Front End", basicColor: "#82CFFA"},
  {id: uuid(), title:"Data Science",basicColor: "#A6D157"},
  {id: uuid(), title:"Devops",basicColor: "#E06B69"},
  {id: uuid(), title:"UX y Diseño", basicColor: "#DB6EBF"},
  {id: uuid(), title:"Móvil", basicColor: "#FFBA05"},
  {id: uuid(), title:"Innovación y Gestión", basicColor: "#FF8A29"}
];

  const [groups,updateGroups] = useState(teams);
  const [showForm,updateShow] = useState(false);
  const [coworkers,updateCoworkers] = useState([...person]);

  const toggleUpdate = () => {updateShow(!showForm)};

  const registerCoworker = (coworker)=>{
    updateCoworkers([...coworkers,coworker])
  }

  const registerTeam = (newTeam) =>{
    updateGroups([...groups, {...newTeam, id: uuid()}]) 
  }

  const updateColor = (color,id) =>{
    // console.log("Actulizar: ", color, title)
      let updated = groups.map( group => {
        if(group.id === id){
          group.basicColor = color;
          group.basicColor = color;
        }
        return group
      });

    return updateGroups(updated);
    
  }

  const deleteCoworker = (id) =>{
    console.log("eliminar: ",id)
    let updated = coworkers.filter((coworker)=> coworker.id !== id);
    updateCoworkers(updated)
  }

  const giveLike = (id) =>{
    console.log("like :",id)
    const updated = coworkers.map((coworker)=>{
      if(coworker.id === id){
        coworker.like = !coworker.like
      }
      return coworker
    })
    updateCoworkers(updated)
  }

  return (
    <div className="App">
      <Header/>
        {
          showForm && <Form teams={ groups.map( (group) => group.title) }
              setWorker = {registerCoworker}
              addTeam = {registerTeam}
            />
        }
      <Myorg toggleShow = { toggleUpdate }/>
        {
          groups.map( (group) => <Team 
              dataTeam  = {group} 
              key       = {group.id}
              coworkers = {coworkers.filter( (coworker) => coworker.team == group.title)}
              deleteCoworker = {deleteCoworker}
              updateColor = {updateColor}
              giveLike = {giveLike}
            />
          )
        }
      <Footer></Footer>
    </div>
  );
}

export default App;
